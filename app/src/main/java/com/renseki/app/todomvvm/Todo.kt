package com.renseki.app.todomvvm

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import com.renseki.app.todomvvm.room.DateConverter
import java.util.*

@Entity
@TypeConverters(DateConverter::class)
data class Todo(
    @PrimaryKey
    val id: String,

    val date: Date,

    val description: String,

    @ColumnInfo(name = "is_finished")
    val isFinished: Boolean
)