package com.renseki.app.todomvvm.room

import android.arch.persistence.room.TypeConverter
import java.util.*

object DateConverter {
    @JvmStatic
    @TypeConverter
    fun dateToLong(date: Date): Long {
        return date.time
    }

    @JvmStatic
    @TypeConverter
    fun longToDate(time: Long): Date {
        return Date(time)
    }
}