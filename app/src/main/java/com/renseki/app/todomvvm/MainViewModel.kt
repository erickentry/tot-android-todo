package com.renseki.app.todomvvm

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel

class MainViewModel(
    todoRepository: TodoRepository
) : ViewModel() {

    val todos = todoRepository.getTodos()

    private val todoToBeInserted = MutableLiveData<Todo>()
    val insertedTodo = Transformations.switchMap(todoToBeInserted) {
        todoRepository.insert(it)
    }

    fun insert(todo: Todo) {
        todoToBeInserted.value = todo
    }
}