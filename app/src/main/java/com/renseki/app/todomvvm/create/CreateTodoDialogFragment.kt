package com.renseki.app.todomvvm.create

import android.app.DatePickerDialog
import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import com.renseki.app.todomvvm.R
import com.renseki.app.todomvvm.Todo
import kotlinx.android.synthetic.main.fragment_create_todo.*
import java.text.SimpleDateFormat
import java.util.*

class CreateTodoDialogFragment : DialogFragment() {

    companion object {
        fun newInstance(): CreateTodoDialogFragment {
            return CreateTodoDialogFragment()
        }
    }

    private lateinit var viewModel: CreateTodoViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_create_todo,
            container,
            false
        )
    }

    override fun onResume() {
        super.onResume()

        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    interface Listener {
        fun onTodoReady(todo: Todo)
    }

    private var listener: Listener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = activity as? Listener
    }

    override fun onDetach() {
        listener = null
        super.onDetach()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = CreateTodoViewModel()

        viewModel.setDate(Date())
        viewModel.setDescription("")

        ed_date.setOnClickListener {
            viewModel.getDate()?.also { selectedDate ->
                showDatePicker(
                    selectedDate
                )
            }
        }

        btn_save.setOnClickListener {
            val description = ed_description.text.toString()
            viewModel.setDescription(description)

            viewModel.submit()?.also { todo ->
                listener?.onTodoReady(todo)
                dismiss()
            }
        }


        viewModel.selectedDate.observe(this, Observer { selectedDate ->
            selectedDate?.apply {
                val dateFormatter = SimpleDateFormat(
                    "EEEE, dd MMMM yyyy",
                    Locale.getDefault()
                )

                val dateString = dateFormatter.format(this)

                ed_date.setText(dateString)
            }
        })

        viewModel.description.observe(this, Observer { description ->
            description?.apply {
                ed_description.setText(this)
            }
        })
    }

    private fun showDatePicker(date: Date) {
        val calendar = Calendar.getInstance()
        calendar.time = date

        val currentYear = calendar.get(Calendar.YEAR);
        val currentMonth = calendar.get(Calendar.MONTH);
        val currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog(
            context,
            { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
                val selectedCalendar = Calendar.getInstance()
                selectedCalendar.set(
                    year,
                    month,
                    dayOfMonth,
                    0,
                    0,
                    0
                )

                viewModel.setDate(selectedCalendar.time)
            },
            currentYear,
            currentMonth,
            currentDayOfMonth
        ).show()
    }
}