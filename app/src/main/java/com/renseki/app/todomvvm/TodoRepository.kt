package com.renseki.app.todomvvm

import android.arch.lifecycle.LiveData

class TodoRepository(
    private val todoLocalService: TodoLocalService
) {
    fun getTodos(): LiveData<List<Todo>> {
        return todoLocalService.getTodos()
    }

    fun insert(todo: Todo): LiveData<Todo> {
        return todoLocalService.insert(todo)
    }
}