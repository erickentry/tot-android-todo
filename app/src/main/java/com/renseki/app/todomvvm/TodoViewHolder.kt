package com.renseki.app.todomvvm

import android.support.v7.widget.RecyclerView
import android.view.View

class TodoViewHolder(
    view: View
) : RecyclerView.ViewHolder(view)
