package com.renseki.app.todomvvm.room

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.renseki.app.todomvvm.Todo

@Dao
interface TodoDao {
    @Query("SELECT * FROM todo")
    fun getTodos(): LiveData<List<Todo>>

    @Insert
    fun insert(todo: Todo)
}