package com.renseki.app.todomvvm.create

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.renseki.app.todomvvm.Todo
import java.util.*

class CreateTodoViewModel : ViewModel() {
    private val _selectedDate = MutableLiveData<Date>()
    val selectedDate: LiveData<Date> = _selectedDate

    private val _description = MutableLiveData<String>()
    val description: LiveData<String> = _description

    fun setDate(date: Date) {
        _selectedDate.value = date
    }

    fun getDate(): Date? {
        return _selectedDate.value
    }

    fun setDescription(description: String) {
        _description.value = description
    }

    fun submit(): Todo? {
        return _selectedDate.value?.let { selectedDate ->
            _description.value?.let { description ->
                createTodo(selectedDate, description)
            }
        }
    }

    private fun createTodo(date: Date, description: String): Todo {
        return Todo(
            id = "",
            date = date,
            description = description,
            isFinished = false
        )
    }
}