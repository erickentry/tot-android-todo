package com.renseki.app.todomvvm

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.renseki.app.todomvvm.room.TodoDao
import java.util.*
import java.util.concurrent.Executors

class TodoLocalService(
    private val todoDao: TodoDao
) {
    fun getTodos(): LiveData<List<Todo>> {
        return todoDao.getTodos()
    }

    fun insert(todo: Todo): LiveData<Todo> {
        val todoWithId = todo.copy(
            id = UUID.randomUUID().toString()
        )

        return MutableLiveData<Todo>().apply {
            Executors.newSingleThreadExecutor().execute {
                todoDao.insert(todoWithId)
                postValue(todoWithId)
            }
        }
    }
}