package com.renseki.app.todomvvm

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView

class TodoListAdapter
    : ListAdapter<Todo, TodoViewHolder>(
    object : DiffUtil.ItemCallback<Todo>() {
        override fun areItemsTheSame(oldItem: Todo, newItem: Todo): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Todo, newItem: Todo): Boolean {
            return oldItem.description == newItem.description
                    && oldItem.isFinished == newItem.isFinished
                    && oldItem.date == newItem.date
        }
    }
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(
            R.layout.activity_main_item,
            parent,
            false
        )
        val viewHolder = TodoViewHolder(view)
        return viewHolder
    }

    override fun onBindViewHolder(viewHolder: TodoViewHolder, position: Int) {
        val todo = getItem(position)
        val view = viewHolder.itemView

        view.findViewById<TextView>(R.id.tv_description).text = todo.description
        view.findViewById<CheckBox>(R.id.cb_finished).isChecked = todo.isFinished
    }
}