package com.renseki.app.todomvvm.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.renseki.app.todomvvm.Todo

@Database(
    entities = [
        Todo::class
    ],
    version = 1
)
abstract class TodoDatabase : RoomDatabase() {
    abstract fun todoDao(): TodoDao
}