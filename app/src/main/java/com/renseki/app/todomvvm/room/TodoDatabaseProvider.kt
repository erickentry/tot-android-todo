package com.renseki.app.todomvvm.room

import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

class TodoDatabaseProvider {
    companion object {
        private var INSTANCE: TodoDatabase? = null
        private const val DB_NAME = "todo_db"

        fun getInstance(context: Context): TodoDatabase {
            return INSTANCE
                ?: Room
                    .databaseBuilder(
                        context.applicationContext,
                        TodoDatabase::class.java,
                        DB_NAME
                    )
                    .build()
                    .apply {
                        INSTANCE = this
                    }

//            if (INSTANCE == null) {
//                INSTANCE = Room
//                    .databaseBuilder(
//                        context.applicationContext,
//                        TodoDatabase::class.java,
//                        DB_NAME
//                    )
//                    .build()
//            }
//            return INSTANCE!!
        }
    }
}