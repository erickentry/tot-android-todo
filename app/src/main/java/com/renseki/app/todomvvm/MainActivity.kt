package com.renseki.app.todomvvm

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.renseki.app.todomvvm.create.CreateTodoDialogFragment
import com.renseki.app.todomvvm.room.TodoDatabaseProvider
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), CreateTodoDialogFragment.Listener {

    private lateinit var adapter: TodoListAdapter
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = MainViewModel(
            TodoRepository(
                TodoLocalService(
                    TodoDatabaseProvider
                        .getInstance(this)
                        .todoDao()
                )
            )
        )

        adapter = TodoListAdapter()
        rv_todos.adapter = adapter

        viewModel.todos.observe(this, Observer { todos ->
            adapter.submitList(todos)
        })

        btn_add.setOnClickListener {
            CreateTodoDialogFragment
                .newInstance()
                .show(
                    supportFragmentManager,
                    "create_todo"
                )
        }

        viewModel.insertedTodo.observe(this, Observer {
            Toast
                .makeText(
                    this,
                    "Todo telah tersimpan",
                    Toast.LENGTH_LONG
                )
                .show()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(
            R.menu.menu_list_todo,
            menu
        )
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            R.id.menu_item_filter -> {
                Toast
                    .makeText(
                        this,
                        "Under Construction",
                        Toast.LENGTH_LONG)
                    .show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onTodoReady(todo: Todo) {
        viewModel.insert(todo)
    }
}
